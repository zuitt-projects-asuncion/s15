
/*- First function: oddEvenChecker
	- This funciton will check if the number input or passed as an argument is an odd or even number.
		-Check if the argumetn being passed is a number or not.
		-even numbers are divisible by 2.
		-log a message in the console: "The number is even." if the number passed as argument is even.
		-log a message in the console: "The number is odd." if the number passed as argument is odd.
		-if the number passed is not a number type:
			show an alert:"Input Invalid."
*/
function oddEvenChecker(num){
	if (typeof num != "number"){
		alert("Invalid Input")
	} // END if num is not a number
	else if(num % 2 === 0){
		console.log("The number is even.")
		}//END if num is even
	else if(num % 2 != 0){
		console.log("The number is odd.")
		}//END if num is odd
} //END function oddEvenChecker

let num = 1.5;

oddEvenChecker(num);



/*
 - Second function: budgetChecker()
 - This function will check if the number input or passed as an argument is over or is less than the recommended budget.
 	-Check if the argument being is a number or not.
 		- if it is a number, check if the nubmer given is greater than 40000
 			-log a message in the console: ("You are over the budget.")
 		- if the number is not over 40000:
 			-log a message in the console: ("You have resources left.")
 		- if the argument passed does not contain a number:
 			- show alert: ("Invalid Input.")
 */


function budgetChecker(budget, recommendedBudget){
	if (typeof budget != "number" && typeof recommendedBudget != "number"){
		alert("Invalid Input")
	} // END if budget and recommdedBudget are not numbers
		else if(budget > recommendedBudget){
			console.log("You are over the budget.")
		} // End if budget > recommdedBudget
		else if(budget < recommendedBudget){
			console.log("You have resources left.")
		}// End if budget > recommdedBudget
}; //END FUNCTION

	let recommendedBudget = "40000";
	let budget = num;

	console.log(budget);
	console.log(recommendedBudget);
	budgetChecker(budget, recommendedBudget);